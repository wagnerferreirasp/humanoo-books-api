# humanoo-books-api

A books api for Humanoo

# Development
Clone this repository and import it into your favorite java IDE as a maven based application. 
For development, you can use the following environment variables and values:

```
DATASOURCE_URL=jdbc:h2:mem:books-api-db
DATASOURCE_DRIVER_CLASS_NAME=org.h2.Driver
DATASOURCE_USERNAME=sa
DATASOURCE_PASSWORD=
JWT_SECRET=notComplexSecret
SECURITY_SAMPLE_USERNAME=user
SECURITY_SAMPLE_PASSWORD=password
```

Check for any other environment variables needed and how they are used looking inside the file `src/main/resources/application.yml`

# Build
You can build it with maven, running the following command inside the base directory:
```sh
mvn clean install
```

# Tests
This project was built with integration and unit tests. Remember to always keep the tests updated; They will keep the development team not afraid to change the code and will make you go faster! You can run the tests in your IDE, but to run the tests via terminal, simply run:
```sh
mvn test
```

# Documentation
The api is documented with Swagger, with a library that automatically generates the documentation based on the controllers of the api.

Swagger url:
- Staging: https://staging-humanoo-books-api.herokuapp.com/swagger-ui.html
- Production: https://humanoo-books-api.herokuapp.com/swagger-ui.html

