FROM openjdk:8-jre-alpine
ADD books-api.jar /opt/books-api.jar

EXPOSE 8080

CMD java -Duser.timezone=America/Sao_Paulo -jar /opt/books-api.jar
