package com.humanoo.booksapi.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.humanoo.booksapi.controller.book.request.BookRequest;
import com.humanoo.booksapi.controller.book.response.BookResponse;
import com.humanoo.booksapi.controller.book.response.MinimalBookResponse;
import org.junit.Assert;
import org.springframework.http.HttpStatus;
import org.springframework.test.web.servlet.MvcResult;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.containsString;
import static org.junit.Assert.assertEquals;

class ControllerTestingUtils {
    private static ObjectMapper objectMapper = new ObjectMapper();

    static void assertStatusIs(HttpStatus expectedStatus, MvcResult mvcResult) {
        assertEquals(expectedStatus.value(), mvcResult.getResponse().getStatus());
    }

    static String getJson(BookRequest bookRequest) throws JsonProcessingException {
        return objectMapper.writeValueAsString(bookRequest);
    }

    static void assertBookWasReturned(BookResponse expectedBookResponse, MvcResult mvcResult) throws IOException {
        String json = mvcResult.getResponse().getContentAsString();
        BookResponse bookResponse = objectMapper.readValue(json, BookResponse.class);
        assertEquals(expectedBookResponse, bookResponse);
    }

    static void assertBooksWereReturned(List<MinimalBookResponse> expectedBooks, MvcResult mvcResult) throws IOException {
        String json = mvcResult.getResponse().getContentAsString();
        List<MinimalBookResponse> actualBooks = Arrays.asList(objectMapper.readValue(json, MinimalBookResponse[].class));
        assertEquals(expectedBooks, actualBooks);
    }

    static void assertResponseContains(String expectedContent, MvcResult mvcResult) throws IOException {
        String json = mvcResult.getResponse().getContentAsString();
        Assert.assertThat(json, containsString(expectedContent));
    }

}
