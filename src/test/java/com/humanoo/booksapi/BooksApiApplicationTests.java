package com.humanoo.booksapi;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class BooksApiApplicationTests {

	@Test
	public void contextLoads() {
	}

	@Test
	public void runMain_ShouldRunWithSuccess() {
		BooksApiApplication.main(new String[]{});
	}
}
