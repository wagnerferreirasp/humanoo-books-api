package com.humanoo.booksapi.mock;

import com.humanoo.booksapi.controller.book.request.BookRequest;
import com.humanoo.booksapi.controller.book.response.AuthorResponse;
import com.humanoo.booksapi.controller.book.response.BookResponse;
import com.humanoo.booksapi.controller.book.response.CategoryResponse;
import com.humanoo.booksapi.controller.book.response.MinimalBookResponse;

import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;

public class BookMockFactory {
    public static BookRequest createBookOK1() {
        return BookRequest.builder()
            .isbn("9783161484100")
            .title("Book 1")
            .authorId(1L)
            .categoryIdList(Collections.singletonList(1L))
            .build();
    }

    public static BookRequest createBookOK2() {
        return BookRequest.builder()
            .isbn("9781234568978")
            .title("Book 2")
            .authorId(2L)
            .categoryIdList(Collections.singletonList(2L))
            .build();
    }

    public static BookRequest createBookWithouAuthorId() {
        return BookRequest.builder()
            .isbn("9783161484100")
            .title("Book 1")
            .categoryIdList(Collections.singletonList(1L))
            .build();
    }

    public static BookRequest createBookWithoutIsbn() {
        return BookRequest.builder()
            .title("Book 1")
            .authorId(1L)
            .categoryIdList(Collections.singletonList(1L))
            .build();
    }

    public static BookRequest createBookWithInexistentAuthor() {
        return BookRequest.builder()
            .isbn("9789877568978")
            .title("Book 3")
            .authorId(9L)
            .categoryIdList(Collections.singletonList(1L))
            .build();
    }

    public static BookResponse createBookResponse1() {
        return BookResponse.builder()
            .id(1L)
            .isbn("123123123123")
            .title("A Title")
            .categoryList(Collections.singletonList(new CategoryResponse(1L, "A name")))
            .author(new AuthorResponse(1L, "A name"))
            .createdAt(new Date(1L))
            .updatedAt(new Date(4L))
            .build();
    }

    public static BookResponse createBookResponse2() {
        return BookResponse.builder()
            .id(2L)
            .isbn("8977787887")
            .title("A Title 2")
            .categoryList(Collections.singletonList(new CategoryResponse(1L, "A name")))
            .author(new AuthorResponse(1L, "A name"))
            .createdAt(new Date(1L))
            .updatedAt(new Date(4L))
            .build();
    }

    public static List<MinimalBookResponse> createListOfMinimalBookResponses() {
        return Arrays.asList(
            MinimalBookResponse.of(createBookResponse1()),
            MinimalBookResponse.of(createBookResponse2())
        );
    }
}
