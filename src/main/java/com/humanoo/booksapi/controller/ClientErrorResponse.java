package com.humanoo.booksapi.controller;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class ClientErrorResponse {
    public static final String BOOK_ISBN_IS_EMPTY = "book.isbn.is.empty";
    public static final String BOOK_TITLE_IS_EMPTY = "book.title.is.empty";
    public static final String BOOK_CATEGORY_LIST_IS_EMPTY = "book.category.list.is.empty";
    public static final String BOOK_AUTHOR_ID_IS_NULL = "book.author.id.is.null";
    public static final String BOOK_DOES_NOT_EXIST = "book.does.not.exist";
    public static final String INCONSISTENT_DATA_CHECK_RELATIONSHIPS = "inconsistent.data.check.relationships";
    public static final String USERNAME_IS_EMPTY = "username.is.empty";
    public static final String PASSWORD_IS_EMPTY = "password.is.empty";
    public static final String USER_OR_PASSWORD_IS_INVALID = "user.or.password.is.invalid";
    public static final String USERNAME_DOES_NOT_EXIST = "user.does.not.exist";

    String clientMessage;
    String developerMessage;

    public static ClientErrorResponse of(String clientMessage, String developerMessage) {
        return new ClientErrorResponse(clientMessage, developerMessage);
    }

    public static ClientErrorResponse singleMessage(String singleMessage) {
        return new ClientErrorResponse(singleMessage, singleMessage);
    }
}
