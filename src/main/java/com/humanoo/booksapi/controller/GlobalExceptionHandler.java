package com.humanoo.booksapi.controller;

import com.humanoo.booksapi.exception.BookNotFoundException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import static com.humanoo.booksapi.controller.ClientErrorResponse.*;
import static com.humanoo.booksapi.controller.ServerErrorResponse.SOMETHING_WENT_WRONG;
import static org.springframework.http.HttpStatus.*;

@RestControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        ObjectError objectError = ex.getBindingResult()
            .getAllErrors()
            .stream()
            .findFirst()
            .orElseThrow(RuntimeException::new);
        String message = objectError.getDefaultMessage();
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
            .body(ClientErrorResponse.singleMessage(message));
    }

    @ExceptionHandler(BookNotFoundException.class)
    @ResponseStatus(NOT_FOUND)
    public ClientErrorResponse handleBookNotFound(BookNotFoundException e){
        return ClientErrorResponse.singleMessage(BOOK_DOES_NOT_EXIST);
    }

    @ExceptionHandler(DataIntegrityViolationException.class)
    @ResponseStatus(BAD_REQUEST)
    public ClientErrorResponse handleConstraintViolation(DataIntegrityViolationException e){
        return ClientErrorResponse.of(INCONSISTENT_DATA_CHECK_RELATIONSHIPS, e.getMessage());
    }

    @ExceptionHandler(Exception.class)
    @ResponseStatus(INTERNAL_SERVER_ERROR)
    public ServerErrorResponse handleException(Exception e) {
        logger.error(e);
        return new ServerErrorResponse(SOMETHING_WENT_WRONG);
    }

    @ExceptionHandler(BadCredentialsException.class)
    @ResponseStatus(BAD_REQUEST)
    public ClientErrorResponse handleBadCredentials(BadCredentialsException e) {
        return ClientErrorResponse.singleMessage(USER_OR_PASSWORD_IS_INVALID);
    }

    @ExceptionHandler(UsernameNotFoundException.class)
    @ResponseStatus(BAD_REQUEST)
    public ClientErrorResponse handleUsernameNotFound(UsernameNotFoundException e) {
        return ClientErrorResponse.singleMessage(USERNAME_DOES_NOT_EXIST);
    }
}
