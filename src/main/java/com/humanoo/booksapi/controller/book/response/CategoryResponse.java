package com.humanoo.booksapi.controller.book.response;

import com.humanoo.booksapi.category.Category;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.stream.Collectors;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CategoryResponse {
    Long id;
    String name;

    public static CategoryResponse fromEntity(Category category) {
        return new CategoryResponse(category.getId(), category.getName());
    }

    public static List<CategoryResponse> fromEntityList(List<Category> categoryList) {
        return categoryList.stream()
            .map(CategoryResponse::fromEntity)
            .collect(Collectors.toList());
    }
}
