package com.humanoo.booksapi.controller.book.request;

import lombok.*;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

import static com.humanoo.booksapi.controller.ClientErrorResponse.*;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
public class BookRequest {
    @NotEmpty(message = BOOK_ISBN_IS_EMPTY)
    String isbn;

    @NotEmpty(message = BOOK_TITLE_IS_EMPTY)
    String title;

    @NotNull(message = BOOK_AUTHOR_ID_IS_NULL)
    Long authorId;

    @NotEmpty(message = BOOK_CATEGORY_LIST_IS_EMPTY)
    List<Long> categoryIdList;
}
