package com.humanoo.booksapi.controller.book;

import com.humanoo.booksapi.book.BookService;
import com.humanoo.booksapi.controller.book.request.BookRequest;
import com.humanoo.booksapi.controller.book.response.BookResponse;
import com.humanoo.booksapi.controller.book.response.MinimalBookResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/books")
public class BookController {
    private BookService bookService;

    @Autowired
    public BookController(BookService bookService) {
        this.bookService = bookService;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public BookResponse insertBook(@Valid @RequestBody BookRequest bookRequest) {
        BookResponse bookResponse = bookService.insertBook(bookRequest);
        return bookResponse;
    }

    @PutMapping("{id}")
    @ResponseStatus(HttpStatus.OK)
    public BookResponse updateBook(@Valid @RequestBody BookRequest bookRequest, @PathVariable Long id) {
        return bookService.updateBook(bookRequest, id);
    }

    @GetMapping("{id}")
    @ResponseStatus(HttpStatus.OK)
    public BookResponse getBookDetails(@PathVariable Long id) {
        return bookService.getBookDetails(id);
    }

    @GetMapping
    public List<MinimalBookResponse> getAllBooks() {
        return bookService.getAllBooks();
    }
}
