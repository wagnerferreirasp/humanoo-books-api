package com.humanoo.booksapi.controller.book.response;

import com.humanoo.booksapi.book.Book;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class MinimalBookResponse {
    Long id;
    String title;
    String authorName;

    public static MinimalBookResponse fromEntity(Book book) {
        return new MinimalBookResponse(
            book.getId(), book.getTitle(), book.getAuthor().getName()
        );
    }

    public static MinimalBookResponse of(BookResponse bookResponse) {
        return new MinimalBookResponse(
            bookResponse.getId(), bookResponse.getTitle(), bookResponse.getAuthor().getName()
        );
    }
}
