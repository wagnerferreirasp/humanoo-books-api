package com.humanoo.booksapi.controller.book.response;

import com.humanoo.booksapi.book.Book;
import lombok.*;

import java.util.Date;
import java.util.List;

@Builder
@AllArgsConstructor
@Data
@NoArgsConstructor
public class BookResponse {
    Long id;
    String isbn;
    String title;
    AuthorResponse author;
    List<CategoryResponse> categoryList;
    Date createdAt;
    Date updatedAt;

    public static BookResponse fromEntity(Book book) {
        return BookResponse.builder()
            .id(book.getId())
            .isbn(book.getIsbn())
            .title(book.getTitle())
            .author(AuthorResponse.fromEntity(book.getAuthor()))
            .categoryList(CategoryResponse.fromEntityList(book.getCategoryList()))
            .createdAt(new Date(book.getCreatedAt()))
            .updatedAt(new Date(book.getUpdatedAt()))
            .build();
    }
}
