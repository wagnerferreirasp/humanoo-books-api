package com.humanoo.booksapi.controller.book.response;

import com.humanoo.booksapi.author.Author;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AuthorResponse {
    Long id;
    String name;

    public static AuthorResponse fromEntity(Author author) {
        return new AuthorResponse(author.getId(), author.getName());
    }
}
