package com.humanoo.booksapi.controller.auth.request;

import lombok.Data;

import javax.validation.constraints.NotEmpty;

import static com.humanoo.booksapi.controller.ClientErrorResponse.PASSWORD_IS_EMPTY;
import static com.humanoo.booksapi.controller.ClientErrorResponse.USERNAME_IS_EMPTY;

@Data
public class LoginRequest {
    @NotEmpty(message = USERNAME_IS_EMPTY)
    String username;
    @NotEmpty(message = PASSWORD_IS_EMPTY)
    String password;
}
