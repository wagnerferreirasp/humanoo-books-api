package com.humanoo.booksapi.controller.auth;

import com.humanoo.booksapi.controller.auth.request.LoginRequest;
import com.humanoo.booksapi.security.AuthService;
import com.humanoo.booksapi.security.Token;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/auth")
public class AuthController {

    @Autowired
    AuthService authService;

    @PostMapping("/login")
    @ResponseStatus(HttpStatus.CREATED)
    public Token login(@RequestBody @Valid LoginRequest loginRequest) {
        return authService.authenticate(loginRequest);
    }
}
