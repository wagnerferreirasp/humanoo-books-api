package com.humanoo.booksapi.book;

import com.humanoo.booksapi.author.Author;
import com.humanoo.booksapi.category.Category;
import com.humanoo.booksapi.controller.book.request.BookRequest;
import com.humanoo.booksapi.controller.book.response.BookResponse;
import com.humanoo.booksapi.controller.book.response.MinimalBookResponse;
import com.humanoo.booksapi.exception.BookNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class BookService {
    private BookRepository repository;
    private EntityManager entityManager;

    @Autowired
    public BookService(BookRepository repository, EntityManager entityManager) {
        this.repository = repository;
        this.entityManager = entityManager;
    }

    public BookResponse insertBook(BookRequest bookRequest) {
        Book book = fillEntityWithDTOData(new Book(), bookRequest);
        Book saved = repository.save(book);
        return BookResponse.fromEntity(findById(saved.getId()));
    }

    public BookResponse updateBook(BookRequest bookRequest, Long id) {
        Book book = fillEntityWithDTOData(findById(id), bookRequest);
        Book saved = repository.save(book);
        return BookResponse.fromEntity(findById(saved.getId()));
    }

    public Book findById(Long id) {
        return repository.findById(id)
            .orElseThrow(BookNotFoundException::new);
    }

    public BookResponse getBookDetails(Long id) {
        Book book = findById(id);
        return BookResponse.fromEntity(book);
    }

    public List<MinimalBookResponse> getAllBooks() {
        return repository.findAll()
            .stream()
            .map(MinimalBookResponse::fromEntity)
            .collect(Collectors.toList());
    }

    private Author getAuthor(BookRequest bookRequest) {
        return entityManager.getReference(Author.class, bookRequest.getAuthorId());
    }



    private List<Category> getCategoryList(BookRequest bookRequest) {
        return bookRequest.getCategoryIdList()
                .stream()
                .map(categoryId -> entityManager.getReference(Category.class, categoryId))
                .collect(Collectors.toList());
    }

    private Book fillEntityWithDTOData(Book book, BookRequest bookRequest) {
        book.setAuthor(getAuthor(bookRequest));
        book.setIsbn(bookRequest.getIsbn());
        book.setCategoryList(getCategoryList(bookRequest));
        book.setTitle(bookRequest.getTitle());
        return book;
    }
}
