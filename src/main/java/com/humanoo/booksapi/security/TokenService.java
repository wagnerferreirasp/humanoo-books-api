package com.humanoo.booksapi.security;

import io.jsonwebtoken.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.Collections;
import java.util.Date;
import java.util.Optional;

@Service
public class TokenService {

    private static final String AUTH_HEADER = "Authorization";
    private static final String TOKEN_TYPE = "Bearer";
    private static final String AUTH_PREFIX = String.format("%s ", TOKEN_TYPE);
    private static final long HOUR_IN_MILLIS = 3600 * 1000L;
    private static final Long JWT_TOKEN_VALIDITY_IN_MILLIS = 6 * HOUR_IN_MILLIS;

    @Value("${jwt.secret}")
    private String secret;

    public Authentication getAuthentication(HttpServletRequest request) {
        Optional<Claims> token = getTokenClaims(request);
        return token.map(this::getUsername)
            .map(username -> new UsernamePasswordAuthenticationToken(username, null, Collections.emptyList()))
            .orElse(null);
    }

    private Optional<Claims> getTokenClaims(HttpServletRequest request) {
        Optional<String> header = Optional.ofNullable(request.getHeader(AUTH_HEADER));
        try {
            return header.map(token -> Jwts.parser()
                .setSigningKey(secret)
                .parseClaimsJws(token.replace(AUTH_PREFIX, ""))
                .getBody());
        } catch (ExpiredJwtException | SignatureException e) {
            return Optional.empty();
        }
    }

    private String getUsername(Claims claims) {
        return claims.getSubject();
    }

    public Token generateToken(UserDetails userDetails) {
        String accessToken = Jwts.builder()
            .setSubject(userDetails.getUsername())
            .setIssuedAt(new Date(System.currentTimeMillis()))
            .setExpiration(new Date(System.currentTimeMillis() + JWT_TOKEN_VALIDITY_IN_MILLIS))
            .signWith(SignatureAlgorithm.HS512, secret)
            .compact();
        return Token.of(TOKEN_TYPE, accessToken);
    }
}
