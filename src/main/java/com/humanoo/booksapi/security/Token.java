package com.humanoo.booksapi.security;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class Token {
    String type;
    String accessToken;

    public static Token of(String type, String accessToken) {
        return new Token(type, accessToken);
    }
}
