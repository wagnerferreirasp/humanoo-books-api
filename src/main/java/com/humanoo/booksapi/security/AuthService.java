package com.humanoo.booksapi.security;

import com.humanoo.booksapi.controller.auth.request.LoginRequest;
import com.humanoo.booksapi.security.Token;
import com.humanoo.booksapi.security.TokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

@Service
public class AuthService {
    @Autowired
    UserDetailsService userDetailsService;
    @Autowired
    TokenService tokenService;
    @Autowired
    private AuthenticationManager authenticationManager;

    public Token authenticate(LoginRequest loginRequest) {
        authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(
            loginRequest.getUsername(), loginRequest.getPassword()
        ));
        UserDetails userDetails = userDetailsService
            .loadUserByUsername(loginRequest.getUsername());
        return tokenService.generateToken(userDetails);
    }
}
