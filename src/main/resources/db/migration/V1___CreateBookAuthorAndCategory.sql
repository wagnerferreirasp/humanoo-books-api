CREATE TABLE author(
    id SERIAL PRIMARY KEY,
    name text NOT NULL
);

CREATE TABLE category(
    id SERIAL PRIMARY KEY,
    name text NOT NULL
);

CREATE TABLE book(
    id SERIAL PRIMARY KEY,
    isbn text NOT NULL,
    title text NOT NULL,
    author_id BIGINT NOT NULL,
    CONSTRAINT "FK_Author_Id" FOREIGN KEY(author_id) REFERENCES author(id)
);

CREATE TABLE book_category(
    id SERIAL PRIMARY KEY,
    book_id BIGINT NOT NULL,
    category_id BIGINT NOT NULL,
    CONSTRAINT "FK_Book_Id" FOREIGN KEY(book_id) REFERENCES book(id),
    CONSTRAINT "FK_Category_Id" FOREIGN KEY(category_id) REFERENCES category(id)
);

